<?php

// Объявление глобального массива для хранения текстов
$textStorage = [];


function add(string $title, string $text): void
{
    global $textStorage; // Объявляем использование глобальной переменной

    $textStorage[] = [
        'title' => $title,
        'text' => $text,
    ];
}


function remove(int $index): bool
{
    global $textStorage;

    if (isset($textStorage[$index])) {
        unset($textStorage[$index]);
        // Чтобы привести ключи массива в порядок после удаления элемента:
        $textStorage = array_values($textStorage);
        return true;
    } else {
        return false;
    }
}

function edit(int $index, string $title, string $text): bool
{
    global $textStorage;

    if (isset($textStorage[$index])) {
        $textStorage[$index]['title'] = $title;
        $textStorage[$index]['text'] = $text;
        return true;
    } else {
        return false;
    }
}

// Тестирование функций
add("Первый заголовок", "Первое сообщение");
add("Второй заголовок", "Второе сообщение");

var_dump($textStorage);

$resultRemove0 = remove(0);
$resultRemove5 = remove(5);

echo "Результат удаления 0: " . ($resultRemove0 ? "true" : "false") . "\n";
echo "Результат удаления 5: " . ($resultRemove5 ? "true" : "false") . "\n";

var_dump($textStorage);

edit(0, "Измененный второй заголовок", "Измененное второе сообщение");
var_dump($textStorage);

// Попытка редактирования несуществующего элемента
$resultEditNonExisting = edit(5, "Новый заголовок", "Новое сообщение");
echo "Редактирование несуществующего элемента: " . ($resultEditNonExisting ? "true" : "false") . "\n";